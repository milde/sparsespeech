import os
from collections import defaultdict
import pylab
import math

model_dirs = '/raid/milde/unspeech_models/sparse_ae/runs/runs/'

dirs = os.listdir(model_dirs)

def frange(start, stop, step, eps=1e-8):
    x = start
    while x + eps < stop:
#        print(x)
        yield x
        x += step

legends = []

for c_dir in dirs:

    model_dir = model_dirs + c_dir + '/tf10'

    num = c_dir[:10]

    files = os.listdir(model_dir)

    print(files)
    
    if 'losses_logfile.txt' in files:
        log_loss_file = model_dir + '/losses_logfile.txt'
    else:
        continue

    if 'tf_param_train' in files:
        tf_param_file = model_dir + '/tf_param_train'
    else:
        continue
    
    my_plot_x = []
    my_plot_y = []

    epochs = defaultdict(int)

    epoch = 0
    first_epoch = True

    do_layer_norm=False
    do_layer_norm_context_vec=False
    do_layer_norm_lstm=False

    rnn_layers = 'none'

    print('opening', tf_param_file)
    with open(tf_param_file) as params:
        for line in params:
            line = line.strip().lower()
            if 'layer_norm' in line:
                if 'do_layer_norm=' in line:
                    do_layer_norm = 'true' in line
                if 'do_layer_norm_context_vec=' in line:
                    do_layer_norm_context_vec = 'true' in line
                if 'do_layer_norm_lstm=' in line:
                    do_layer_norm_lstm = 'true' in line
                print(line)

            if 'rnn_layers' in line:
                print(line)
                rnn_layers = line.split('=')[1]


    legend = num + '_' + ("layer_norm_full" if (do_layer_norm and do_layer_norm_context_vec and do_layer_norm_lstm) else ('layer_norm_pratial' 
        if (do_layer_norm or do_layer_norm_context_vec or do_layer_norm_lstm) else 'vanilla')) + '_l' + rnn_layers

    print(legend)

    with open(log_loss_file) as log_loss:
        for line in log_loss:
            split = line.split()
            loss = float(split[6])
            epoch = int(split[3])

            if first_epoch:
                print(epoch)
                print(loss)
                first_epoch=False

            my_plot_y.append(math.log(loss))

            epochs[epoch] += 1

#    print(epoch)

    if epoch != 0:
        for n in range(epoch+1):
#            print(n)
#            print(epochs)
#            print(1.0/epochs[n])
            epoch_list = list(frange(n,n+1.0,1.0/epochs[n]))
#            print(len(epoch_list))
            assert(len(epoch_list) == epochs[n])
#            print("assert(len(epoch_list) == epochs[n]) ok")
            my_plot_x += frange(n,n+1,1.0/epochs[n])

#        print("len myplot_x:,", len(my_plot_x))
#        print("sum: ", sum([epochs[e] for e in epochs]))

        pylab.plot(my_plot_x, my_plot_y, label=legend)

pylab.xlabel('epoch')
pylab.ylabel('loss')
pylab.title('pretrain models (librilight)')
pylab.legend()

pylab.savefig("plots/pretrain_models.pdf", bbox_inches='tight')
