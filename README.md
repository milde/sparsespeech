# SparseSpeech: Unsupervised Acoustic Unit Discovery with Memory-Augmented Sequence Autoencoders.

You can use sparsespeech to train an unsupervised acoustic modell on unlabelled speech data. See also:

"SparseSpeech: Unsupervised Acoustic Unit Discovery with Memory-Augmented Sequence Autoencoders." Benjamin Milde and Chris Biemann. In: Proceedings of Interspeech 2019, Graz, Austria

The reference implementation for this paper contains the following scripts:

* Main training program (sparse_seq_ae.py)
* Kaldi IO functions to read scp and ark feature files (kaldi_io.py)
* Sparsity-inducing memory component (memory.py)
* Separate program to convert Kaldi phone alignments into word alignments (get_word_alignments.py)
* common util functions (myutils.py)

## Training procedure:

Unfortunatly, training will converge to a degenerate solution of using one or two unit of the memory bank for all if you start training the network end-to-end with the memory component. You have to first train a network without the sparsity constraint and memory network:

```bash
mkdir -p unspeech_models/sparse_ae/runs/

python3 sparse_seq_ae.py --train_dir unspeech_models/sparse_ae/runs/ --spk2utt '' --ali_word_ctm '' --ali_ctm '' --phones '' --filelist librispeech_360/plp_360_feats.ark --noscan_sparsity_values --nopylab_show_feats --pylab_savefigs_pdf True --memory_augment_epoch 300 --train_epochs 30 --rnn_hidden_cells 512 -nosmooth_feats --rnn_layers 4 --modelname plp_pretrain --max_seq_len 2000 --batch_size 16 --noevaluate
```

This will also create a new training directory in "unspeech_models/sparse_ae/runs/", with some parameters in the folder and the unix timestamp to make a unique directory for this run. The model is periodically saved to this destination. If you run this in an ipython notebook environment or an IDE like spyder that embeds matplotlib plots, you can also enable debug example outputs of the network (--nopylab_show_feats vs. --pylab_show_feats). PDF files can also be periodically saved with some example outputs of the network containing the same plots (--pylab_savefigs_pdf, useful for debugging on a server without graphical interface).

After you've succesfully trained the network, you can start a new training run with the memory component referencing the training dir with $pretrainmodel of the previous run:

```bash
export pretrainmodel="/path/to/pretrain_model"
python3 sparse_seq_ae.py --modelname "plp_final" --filelist librispeech_360/plp_360_feats.ark --nopylab_show_feats --pylab_savefigs_pdf True --memory_augment_epoch 0 --train_epochs 10 --rnn_hidden_cells 256 --train_dir $pretrainmodel --phones librispeech_360/phones.txt --ali_ctm librispeech_360/alignments_360.txt --ali_word_ctm librispeech_360/word_alignments_360.txt --spk2utt librispeech_360/spk2utt --ABX_eval_samples 400 --nosmooth_feats --nodtw_normalized --rnn_layers 2 --batch_size 32 --cluster_algo sklearn_kmeans --memory_init_samples 1000 --memory_subnetwork_trainingsteps 6000 --filelist_context_features '' --num_mem_entries 16 --sparsity_regularizer_scale 2.0 --diversity_regularizer_scale 100.0 --diversity_loss_type kl
```

The weights of all layers are copied over, except the memory subnetwork that does not exist in the pre-trained network. The arguments  "--cluster_algo sklearn_kmeans --memory_init_samples 1000 --memory_subnetwork_trainingsteps 6000" will control the way the memory module is initialized. In this example, the sklearn_kmeans implementation is used to cluster the data points, 1000 utterances are drawn at random from the corpus to generate the vectors that are clustered with kmeans and the memory subnetwork is trained for 6000 steps with rmsprop.

This also sets the memory component to parameters that have produced good results ("--num_mem_entries 16 --sparsity_regularizer_scale 2.0 --diversity_regularizer_scale 100.0"), see the paper for details.

Note that you do not need to supply speaker and alignment information as they are not used for training the network, but if you do, you can directly evaluate the model after it is trained with the "--evaluate" switch. If you use "--evaluate", the training dir you specify is loaded and evaluated. If you want to evaluate ABX scores directly after training without rerunning the script, you can also use the "--evaluate_after_train" switch. If you want to train the network on data without reference alignment information, simply set --spk2utt '' --ali_word_ctm '' --ali_ctm '' --phones '' instead of actual files. This will disable any evaluation.

## Evaluating

The evaluation is based on the ABX measure on phones and words, with across and within speaker variants. See https://zerospeech.com/2017/track_1.html for details. 

### Generating alignments

The alignment format for phones is one item per line, with id, placeholder, start time in seconds, duration in seconds and unit index:

```csv
100-121669-0000 1 0.000 0.300 1
100-121669-0000 1 0.300 0.140 28
100-121669-0000 1 0.440 0.180 21
```

Phone alignments can be created with Kaldi models by using force-alignment. There is a get_alignments.sh script included in the Sparsespeech repository. You have to copy it to your Kaldi egs and change the paths to match your setup, for example for Librispeech it is:

```bash
. path.sh
  
ali=tri5b_ali_clean_360

for i in exp/$ali/ali.*.gz;
do
  ali-to-phones --ctm-output exp/$ali/final.mdl ark:"gunzip -c $i|" -> ${i%.gz}.ctm;
done;

cat exp/$ali/*.ctm > exp/$ali/merged_alignment.txt
```

Word alignments have the following format:

```csv
100-121669-0000 1 0.300 0.390 TOM T AA1 M
100-121669-0000 1 0.690 0.090 THE DH AH1
100-121669-0000 1 0.780 0.490 PIPER'S P AY1 P ER0 Z
100-121669-0000 1 1.270 0.550 SON S AH1 N
100-121669-0001 1 0.170 0.120 THE DH AH0
100-121669-0001 1 0.290 0.300 PIG P IH1 G
100-121669-0001 1 0.590 0.230 WAS W AH0 Z
100-121669-0001 1 0.820 0.270 EAT IY1 T
100-121669-0001 1 1.090 0.230 AND AE1 N D
100-121669-0001 1 1.320 0.300 TOM T AA1 M
```

Again, one item per line id, placeholder, start time in seconds, duration in seconds and the word followed by its phonetic transcription.

To convert a phone alignment into a word alignment, use the get_word_alignments.py program and match the paths at the beginning of the file to your setup.

### Evaluate baseline features

Sparse_seq_ae.py can also be used to evaluate baseline features (MFCC, PLP, ...). For example if you have stores features (plp_360_feats.ark) and auxillary files (spk2utt,alignments_360.txt,word_alignments_360.txt,phones.txt) librispeech_360/ then you can run:

```bash
python3 sparse_seq_ae.py --evaluate --baseline_feat_eval --modelname plp --filelist librispeech_360/plp_360_feats.ark --spk2utt librispeech_360/spk2utt --ali_ctm librispeech_360/alignments_360.txt --ali_word_ctm librispeech_360/word_alignments_360.txt
--phones librispeech_360/phones.txt --nodtw_normalized --device /cpu:1
```

### Sweep parameter space:

You can append --scan_sparsity_values to a training run. This means that multiple training runs are conducted and the results are written into the four csv files "results_file_triphn_ABX_across.csv", "results_file_triphn_ABX_within.csv" , "results_file_word_ABX_across.csv" and "results_file_word_ABX_within.csv". Currently, there are hard coded lists at the end of sparse_seq_ae.py under the line "FLAGS.scan_sparsity_values:". We will change this in later versions so that it is accessible from the command line.

### Generating labels

You can generate labels in Kaldi format for an input ark or scp. You use the -filelist parameter for the input ark or scp and specify a trained model with --train_dir. You'll also need to set "--evaluate" and "--gen_kaldi_file text_train" and you can disable any alignment information, e.g. use  --ali_ctm "" --ali_word_ctm "" --phones "". A complete example is:

```bash
train_dir=/srv/data/milde/unspeech_models/sparse_ae/runs/runs/1553820097sparsespeech_plp_alictm__bs32_lstmdim256_lstmlayers2_embdim32_num_mem_entries80_sparsity_reg200.0_diversity_reg0.0_dwelling_reg0.0_ccconcat_seq_dropoutkeep0.33_max_epochs30/resumed/1555434165sparsespeech_plp_dpgmm_new_alictm_alignments_360.txt_bs32_lstmdim256_lstmlayers2_embdim32_num_mem_entries16_cluster__sparsity_reg2.0_diversity_reg10.0_dwelling_reg0.0_ccconcat_seq_dropoutkeep0.33_max_epochs10/

python3 sparse_seq_ae.py --evaluate --train_dir $train_dir --nopylab_show_feats --modelname plp_zs_360 --device /gpu:1 --nodtw_normalized -filelist feats/librispeech_360/plp_360_feats.ark --ali_ctm "" --ali_word_ctm "" --phones "" --filelist_context_features "" --noDPGMM_cluster_baseline --nobaseline_feat_eval --num_mem_entries 16 --batch_size 16 --max_seq_len 200000 --gen_kaldi_file text_train
```

### Visulatization

If you use:

```bash
--demo_utterance_num 420 --pylab_savefigs_pdf 
```

Then a pdf file will be saved to the training dir and is periodically updated with a plot of utterance number 420 and one from the current batch. 

### Multi GPU support

There is experimental multi GPU support. Different parts of the model can be placed onto different GPUs (this also means a bigger model than what would fit on a single GPU can be optimized). For example, if you use:

For example, this places the encoder on one GPU and the decoder on a second GPU:

```bash
--encoder_device "/gpu:1" --bridge_device "/gpu:1" --decoder_device "/gpu:2" --loss_device "/gpu:2"
```

# References

    @inproceedings{milde19_interspeech,
      author={Benjamin Milde and Chris Biemann},
      title={{SparseSpeech: Unsupervised Acoustic Unit Discovery with Memory-Augmented Sequence Autoencoders}},
      year=2019,
      booktitle={Proc. Interspeech 2019},
      pages={256--260},
      address={Graz, Austria},
      doi={10.21437/Interspeech.2019-2938}
    }

    @inproceedings{milde20_interspeech,
      author={Benjamin Milde and Chris Biemann},
      title={{Improving Unsupervised Sparsespeech Acoustic Models with Categorical Reparameterization}},
      year=2020,
      booktitle={Proc. Interspeech 2020},
      pages={2747--2751},
      address={Virtual Shanghai, China},
      doi={10.21437/Interspeech.2020-2629}
    }

