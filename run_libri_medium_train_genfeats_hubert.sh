model_dir=/raid/milde/unspeech_models/sparse_ae/runs/runs/1580209758sparsespeech_plp512_x2_pretrain_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems32_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.001_ccconcat_dkeep0.33_epochs10_lrelu/

# 4 layers, 256 dim, 1 epoch trained on 6k hours
model_dir=/raid/milde/unspeech_models/sparse_ae/runs/runs/1587143813sparsespeech_plp256_x4_pretr_med_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems32_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.001_ccconcat_dkeep0.33_epochs1_lrelu/

model_dir=/raid/milde/unspeech_models/sparse_ae/runs/runs/1587143813sparsespeech_plp256_x4_pretr_med_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems32_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.001_ccconcat_dkeep0.33_epochs1_lrelu/resumed/1587479735sparsespeech_plp_medium_x4_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems42_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.1_ccconcat_dkeep0.33_epochs2_lrelu/

model_dir=/raid/milde/unspeech_models/sparse_ae/runs/runs/1587143813sparsespeech_plp256_x4_pretr_med_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems32_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.001_ccconcat_dkeep0.33_epochs1_lrelu/resumed/1589333027sparsespeech_plp_medium_x4_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems100_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.1_ccconcat_dkeep0.33_epochs2_lrelu/

model_dir=/raid/milde/unspeech_models/sparse_ae/runs/runs/1587143813sparsespeech_plp256_x4_pretr_med_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems32_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.001_ccconcat_dkeep0.33_epochs1_lrelu/resumed/1596563187sparsespeech_plp_medium_x4_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems42_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.1_ccconcat_dkeep0.33_epochs2_lrelu_hubner/

model_dir=/raid/milde/unspeech_models/sparse_ae/runs/runs/1587143813sparsespeech_plp256_x4_pretr_med_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems32_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.001_ccconcat_dkeep0.33_epochs1_lrelu/resumed/1596563187sparsespeech_plp_medium_x4_alictm__bs8_lstmdim256_lstmlayers4_embdim32_num_mems42_cluster__spars_reg0.0_div_reg100.0_kl_dw_reg0.0_gumbel_softmax_gg0.1_ccconcat_dkeep0.33_epochs2_lrelu_hubner/

gumbel_generate_temperature=2.0
num_mem_entries=42

out_dir=/raid/milde/libri-light/4hubert/final_256_x4_model_medium_dataset_small_huber_n${num_mem_entries}_g$gumbel_generate_temperature/

mkdir -p $out_dir

echo out dir: $out_dir

python3.8 sparse_seq_ae.py --modelname "plp_medium_x4" --train_dir $model_dir --spk2utt '' --ali_word_ctm '' --ali_ctm '' --filelist /raid/milde/libri-light/unlab_data/small_cut_kaldi/unnormalized_feats.ark  --noscan_sparsity_values --rnn_hidden_cells 256 -nosmooth_feats --rnn_layers 4 --max_seq_len 2000 --batch_size 8 --evaluate --num_mem_entries $num_mem_entries --sparsity_regularizer_scale 0.0 --diversity_regularizer_scale 100.0 --diversity_loss_type kl  --gumbel_generate_temperature $gumbel_generate_temperature --memory_softmax_type softmax --gen_hubert_dir $out_dir --utt2file /raid/milde/libri-light/unlab_data/small_cut_kaldi/utt2file --corpus_base_dir /raid/milde/libri-light/ #--gen_bypass #--do_layer_norm --do_layer_norm_context_vec --do_layer_norm_lstm 

#python3 sparse_seq_ae.py --modelname "plp_gumbel" --filelist ${librispeech_folder}data/train_clean_360_plp/plp_360_feats.ark --nopylab_show_feats --pylab_savefigs_pdf True --memory_augment_epoch 0 --train_epochs 10 --rnn_hidden_cells 256 --train_dir ${train_dir}/${model} --phones ${librispeech_folder}exp/tri5b_ali_clean_360/phones.txt --ali_ctm ${librispeech_folder}/alignments_360.txt --ali_word_ctm ${librispeech_folder}/word_alignments_360.txt --spk2utt ${librispeech_folder}/data/train_clean_360/spk2utt --ABX_eval_samples 400 --nosmooth_feats --nodtw_normalized --evaluate_after_train --rnn_layers 2 --batch_size 32 --cluster_algo sklearn_kmeans --memory_init_samples 1000 --memory_subnetwork_trainingsteps 6000 --filelist_context_features '' --num_mem_entries 16 --sparsity_regularizer_scale 0.0 --diversity_regularizer_scale 100.0 --diversity_loss_type kl  --gumbel_temperature_start 2.0 --gumbel_temperature_stop 0.1 --gumbel_generate_temperature 0.1 --memory_softmax_type gumbel_softmax

