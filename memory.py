#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 18:02:53 2018
Memory and sparsifying layer
@author: Benjamin Milde
"""

license = '''

Copyright 2018, 2019 Benjamin Milde (Language Technology, Universität Hamburg, Germany)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.'''

import numpy as np
import math
import tensorflow as tf
from tensorflow.python.ops import array_ops, math_ops, nn, nn_ops, random_ops

from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops

# adapted from
# https://github.com/tensorflow/tensorflow/blob/r1.14/tensorflow/contrib/distributions/python/ops/relaxed_onehot_categorical.py,
# function   def _sample_n(self, n, seed=None)

def gumbel_softmax(logits, temperature, gumbel_noise_weight, seed=None, eps=1e-10, dtype=tf.float32):
    
    # this is only needed if we sample n instances:
    
    #sample_shape = array_ops.concat([[n], array_ops.shape(self.logits)], 0)
    #logits = self.logits * array_ops.ones(sample_shape, dtype=self.dtype)
    #logits_2d = array_ops.reshape(logits, [-1, self.event_size])
    
    # Uniform variates must be sampled from the open-interval `(0, 1)` rather
    # than `[0, 1)`. To do so, we use `np.finfo(self.dtype.as_numpy_dtype).tiny`
    # because it is the smallest, positive, "normal" number. A "normal" number
    # is such that the mantissa has an implicit leading 1. Normal, positive
    # numbers x, y have the reasonable property that, `x + y >= max(x, y)`. In
    # this case, a subnormal number (i.e., np.nextafter) can cause us to sample
    # 0.
    uniform = random_ops.random_uniform(
        shape=array_ops.shape(logits),
        minval=np.finfo(np.float16).tiny,
        maxval=1.,
        dtype=dtype,
        seed=seed)
    gumbel = gumbel_noise_weight * (-math_ops.log(-math_ops.log(uniform)))
    
    noisy_logits = math_ops.div(logits+gumbel, temperature)
    noisy_softmax = tf.nn.softmax(noisy_logits)

    return noisy_softmax, noisy_logits


def sparsemax(logits, name=None):
  """Computes sparsemax activations [1].
  For each batch `i` and class `j` we have
    $$sparsemax[i, j] = max(logits[i, j] - tau(logits[i, :]), 0)$$
  [1]: https://arxiv.org/abs/1602.02068
  Args:
    logits: A `Tensor`. Must be one of the following types: `half`, `float32`,
      `float64`.
    name: A name for the operation (optional).
  Returns:
    A `Tensor`. Has the same type as `logits`.
  """

  with ops.name_scope(name, "sparsemax", [logits]) as name:
    logits = ops.convert_to_tensor(logits, name="logits")
    obs = array_ops.shape(logits)[0]
    dims = array_ops.shape(logits)[1]

    z = logits - math_ops.reduce_mean(logits, axis=1)[:, array_ops.newaxis]

    # sort z    
    z_sorted, _ = nn.top_k(z, k=dims)
    # calculate k(z)
    z_cumsum = math_ops.cumsum(z_sorted, axis=1)
        
    k = math_ops.range(
        1, math_ops.cast(dims, logits.dtype) + 1, dtype=logits.dtype)
    z_check = 1 + k * z_sorted > z_cumsum
    # because the z_check vector is always [1,1,...1,0,0,...0] finding the
    # (index + 1) of the last `1` is the same as just summing the number of 1.
    k_z = math_ops.reduce_sum(math_ops.cast(z_check, dtypes.int32), axis=1)

    # calculate tau(z)
    indices = array_ops.stack([math_ops.range(0, obs), k_z - 1], axis=1)
    tau_sum = array_ops.gather_nd(z_cumsum, indices)
    tau_z = (tau_sum - 1) / math_ops.cast(k_z, logits.dtype)

    # calculate p
    return math_ops.maximum(math_ops.cast(0, logits.dtype), z - tau_z[:, array_ops.newaxis])

def memory_augment(query, key_memory_W ,key_memory, value_memory, print_tensor_dims=True,
                   softmax_type='softmax', sparsity_regularizer_norm='l2', pre_relu=False,
                   euclidean_normalization=False, matmul=True, with_tanh=False,
                   gumbel_temperature=1.0, gumbel_noise_weight=0.0, dtype=tf.float32):
    '''
    Args:
        
        query: 2D-Tensor, (?, querysize)
        key_memory: 2D-Tensor, (num_cells, cell_size)
        value_memory: 2D-Tensor, (num_cells, cell_size)
        softmax_type: softmax, sparsemax, gumbel_softmax
        print_tensor_dims: will print debug information about tensor shapes if set to True
        sparsity_regularizer_norm: one of "l2", "infinity" or "none"
        gumbel_temperature: only applicable if softmax_type = gumbel_softmax
        
        You can also set key_memory = value_memory
    
    Returns:
        
        context_mem_out: Memory augmented tensor output, same dimensions as query.
        query_out: Memory adressing output, that can be interpreted as soft-clustering over the memory values.
        regularizer_loss: can be used to control sparseness of query_out, if added to the networks loss function.
        
    '''   

    if print_tensor_dims:
        print('query shape:', query.get_shape())
    
    # Prepare dot product by multipling every key_memory element with the query.
    # We add a single empty dimension to the tensor for correct broadcasting (?, 1, querysize).
    if matmul:
        mul_query_key_memory = tf.matmul(query, key_memory_W) + key_memory  
    else:
        if euclidean_normalization:
            #query_norms = tf.norm(query, axis=1, ord='euclidean', keepdims=True)
            keymem_norms = tf.norm(key_memory, axis=1, ord='euclidean', keepdims=True)
            mul_query_key_memory = tf.multiply(array_ops.expand_dims(query , 1), #/ query_norms
                                               key_memory / keymem_norms)
        else:
            mul_query_key_memory = tf.multiply(array_ops.expand_dims(query, 1), key_memory)
    
    if with_tanh:
        tf.nn.tanh(mul_query_key_memory)
    if print_tensor_dims:
        print('mul_query_key_memory shape', mul_query_key_memory.get_shape())
    
    # This would be an alternative, but tensordot is not GPU compatible at the moment
    # query_out = tf.nn.softmax(tf.tensordot(query, memory, axes=1))
    if matmul:
        mul_query_key_memory_reduced = mul_query_key_memory
    else:
        mul_query_key_memory_reduced = tf.reduce_sum(mul_query_key_memory, 2, keep_dims=False)

    
    
    # TODO: divide mul_query_key_memory_reduced by magnitude of the query and key memory -> cosine distance
    
    if pre_relu:
        mul_query_key_memory_reduced = tf.nn.relu(mul_query_key_memory_reduced)
    
    # Now we compute the softmax over the dot product between query x every element in the key memory
    if softmax_type=='sparsemax':
        query_out = sparsemax(mul_query_key_memory_reduced)
    elif softmax_type=='gumbel_softmax':
        query_out, noisy_logits = gumbel_softmax(logits=mul_query_key_memory_reduced,
                                                 temperature=gumbel_temperature,
                                                 gumbel_noise_weight=gumbel_noise_weight,
                                                 dtype=dtype)
        #mul_query_key_memory_reduced /= gumbel_temperature
    elif softmax_type=='softmax':
        query_out = tf.nn.softmax(mul_query_key_memory_reduced)
    elif softmax_type=='log_softmax':
        query_out = nn_ops.log_softmax(mul_query_key_memory_reduced+(1.0+1e-10))
    else:
        print('Softmax type:', softmax_type, 'not supported in memory cell.')
        sys.exit(-1000)
        
    if print_tensor_dims:
        print('query_out/key memory shape', query_out.get_shape(), key_memory.get_shape())
    
    # 1.0 - l2 loss without sqrt
    if sparsity_regularizer_norm == 'l2':
        regularizer_loss = tf.reduce_sum(1.0 - tf.reduce_sum(tf.multiply(query_out, query_out), axis=1))
    elif sparsity_regularizer_norm == 'l2_bs':
        regularizer_loss = 128.0 - tf.reduce_sum(tf.multiply(query_out, query_out))
    elif sparsity_regularizer_norm == 'l2_bug':
        regularizer_loss = tf.reduce_sum(1.0 - tf.multiply(query_out, query_out))
    elif sparsity_regularizer_norm == 'infinity':
        # apply 1.0 - infinity norm to every vector in the batch, then sum over the whole batch
        regularizer_loss = tf.reduce_sum(1.0 - tf.reduce_max(query_out, axis=1)) #reduction_indices=[1]))
    elif sparsity_regularizer_norm == 'none':
        regularizer_loss = 0.0
    else:
        print("Warning", sparsity_regularizer_norm, "not supported.")
        regularizer_loss = 0.0
    
    context_out = query_out 
    
    # We now use the query output (softmax) to multiply over the value_memory. 
    # Expand 0 dimension for value memory (broadcasting for batch size)
    # Expand last dimension of context_out, to multiply over full vectors (broadcasting)
    if print_tensor_dims:
        print('context_out/value memory shape:', context_out.get_shape(), value_memory.get_shape())
        
    context_mem_out = array_ops.expand_dims(value_memory, 0) * array_ops.expand_dims(context_out, -1)
    
    if print_tensor_dims:
        print('context_memory_out shape before reduce:', context_mem_out.get_shape())
        
    context_mem_out = math_ops.reduce_sum(context_mem_out, [1])
    
    if print_tensor_dims:
        print('context_out shape (after reduce):', context_mem_out.get_shape())
    
    if softmax_type=='gumbel_softmax':
        return context_mem_out, query_out, noisy_logits, regularizer_loss

    return context_mem_out, query_out, mul_query_key_memory_reduced, regularizer_loss


def memory_combine(context_mem_out, original_input, mode='pass_through', _context_mul=None, _input_mul=None):
    if mode == 'pass_through':
        context_combined = original_input
    elif mode == 'cluster':
        context_combined = context_mem_out
    elif mode == 'combined':   
        context_combined = context_mem_out + original_input
    elif mode == 'tensor_mul_combined':
        context_combined = _context_mul*context_mem_out + _input_mul * original_input
    else:
        print('Mode:', mode, 'not supported.')
        
    return context_combined 
