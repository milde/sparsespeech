python3 sparse_seq_ae.py --modelname 'plp_nocmvn' --evaluate --DPGMM_baseline_model "" --DPGMM_cluster_baseline --nopylab_show_feats --modelname plp_DPGMM --device /cpu:1 --nodtw_normalized --filelist feats/librispeech_360/plp_360_feats.ark --filelist_context_features ""

#python3 sparse_seq_ae.py --evaluate --baseline_feat_eval --modelname fbank --filelist feats/librispeech_360/train_clean_360.ark --nodtw_normalized --device /cpu:1
#python3 sparse_seq_ae.py --evaluate --baseline_feat_eval --modelname mfcc --filelist feats/librispeech_360/mfcc_hires_360.ark --nodtw_normalized --device /cpu:1

